package AnnotationsReflection;

import java.lang.reflect.Method;

public class ExampleReflection {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		
		printMethods("java.lang.String");
	}

	private static void printMethods(String string) throws ClassNotFoundException {	
		Class c = Class.forName(string);
		for (Method m : c.getMethods()) {
			System.out.print(m.getReturnType().
					getSimpleName() + " ");
				System.out.print(m.getName() + "(");
				String delim = "";
				for (Class param : m.getParameterTypes()) {
					System.out.print(delim);
					System.out.print(param.getSimpleName());
					delim = ", ";
				}
		System.out.println(")");
		}
		
	}
}
