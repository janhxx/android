package AnnotationsReflection;

import java.lang.reflect.Method;

public class EigeneAnnotationen {
	@Autor (vorname  = "Carl", 
			nachname = "Coder")

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Auto a = new Auto(10, 10, "YOLOCAR");
		System.out.println(a.getClass());
		for (Method m : a.getClass().getMethods()) {
			if (m.isAnnotationPresent(Autor.class)) {
				Autor autor = m.getAnnotation(Autor.class);
				System.out.println("Methode '" +m.getName()+ "' entwickelt von "
					+ autor.vorname() + " "
					+ autor.nachname());
			}
		}		
	}
}
