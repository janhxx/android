package AnnotationsReflection;

import java.lang.annotation.*;

// f�r Reflection (Zugriff zur Laufzeit):
@Retention(RetentionPolicy.RUNTIME)

public @interface Autor {
	String vorname();
	String nachname();
	String firma() default "Stark Industries";
}
