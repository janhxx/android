package AnnotationsReflection;

public class Auto {
	private int size,baujahr;
	private String name;
	
	@Autor (vorname  = "Jay", 
			nachname = "Swagstar")
	public Auto(int size, int baujahr, String name) {
	
		this.size = size;
		this.baujahr = baujahr;
		this.name = name;
	}
	
	@Autor (vorname  = "Jayl", 
			nachname = "Coder")
	public int getSize() {
		return size;
	}
	
	@Autor (vorname  = "Carl", 
			nachname = "Swagstar")

	public int getBaujahr() {
		return baujahr;
	}
	
	@Autor (vorname  = "Carl", 
			nachname = "Coder")
	public String getName() {
		return name;
	}
}
