package Threads;

public class CounterThread extends Thread {
	private SlowCounter counter;

	public CounterThread(String name, SlowCounter counter) {
		super(name);
		this.counter = counter;
	}

	@Override
	public void run() {
		while (counter.getValue()==0 || counter.getValue()<10) {
			counter.increase();
			System.out.format("Thread %s: %d %n", getName(), counter.getValue());
		}
	}
}