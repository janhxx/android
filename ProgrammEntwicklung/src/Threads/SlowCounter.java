package Threads;

public class SlowCounter {
	private int value = 0;
	
	
	
	public synchronized void increase(){
		int tmp = getValue();
		try {
		Thread.sleep(200); // Sleep 200 ms
		} catch (InterruptedException e) { }
		setValue(tmp + 1);
	}
	
	public int getValue(){
		return value;
	}
	
	public void setValue(int i){
		value = i;
	}
}
