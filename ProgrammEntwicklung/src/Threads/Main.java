package Threads;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		SlowCounter counter = new SlowCounter();
		CounterThread t1 = new CounterThread("T1", counter);
		CounterThread t2 = new CounterThread("T2", counter);
		t1.start();
		t2.start();
	}
}
