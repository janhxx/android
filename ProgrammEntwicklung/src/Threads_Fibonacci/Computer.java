package Threads_Fibonacci;

public class Computer {
	
	public static synchronized int computeFib(int zahl){
		try {
			Thread.sleep(10); // Sleep 200 ms
			} catch (InterruptedException e) { }
		if(zahl==0)return 0;
		if(zahl==1)return 1;
		
		else return computeFib(zahl-2)+computeFib(zahl-1);
	}
}
