package Threads_Fibonacci;


public class FibonacciThread extends Thread {

	int count = 0;
	private String name;

	public FibonacciThread(String name) {
		this.name = name;
		//super(name);
	}
	
	@Override
	public void run(){
		while(count<20){
			System.out.println(name + " - "+ Computer.computeFib(count));
			count = count + 1;
		}
	}
}
