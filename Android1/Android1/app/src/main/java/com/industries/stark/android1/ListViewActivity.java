package com.industries.stark.android1;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.ArraySet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Jan on 09.10.2015.
 */
public class ListViewActivity extends ListActivity {

    private final String KEY = "content";
    private final int MAXHISTORY = 30;
    HashMap<String, Integer> content = new HashMap<String, Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("RECEIVER", "onCreate");
        Intent calling = getIntent();
        String msg = calling.getStringExtra(InputActivity.EXTRA_MSG);
        Log.d("RECEIVER", "got msg:" + msg);
        content.put(msg.toUpperCase(), Color.BLACK);
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);


        //Nur die ersten n Elemente der Liste hinzufügen
        for (int n = 1; n <=MAXHISTORY; n++) {
            String entry = sharedPreferences.getString(KEY + n, null);
            if (entry != null) {
                //Wir wollen keine leeren Einträge in der Liste
                if(entry.equals("")){
                    Log.d("empty", "EMPTY");
                    entry = "LEER";
                }
                //Und auch keine doppelten
                if(content.containsKey(entry)){
                    Log.d("contains", entry);
                    entry = entry + System.currentTimeMillis();
                }
                 content.put(entry, Color.BLACK);
            } else {
                break;
            }
        }

        ArrayList keyList = new ArrayList(content.keySet());
        ArrayAdapter<String> adaptor = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, keyList);
        setListAdapter(adaptor);
    }


    @Override
    protected void onDestroy() {
        //Vorher noch die Liste in die Preferences speichern
        int n = 1;
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        for (Map.Entry<String, Integer> entry : content.entrySet()) {
            editor.putString(KEY + n, entry.getKey());
            n++;
            Log.d("RECEIVER",entry.getKey());
        }
        editor.commit();
        super.onDestroy();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        TextView child = (TextView) l.getChildAt(position);
        child.setTextColor(Color.RED);
        String text = child.getText().toString();
        Log.d("text", text);
        if(content.get(text) != null){
            if(content.get(text).equals(Color.BLACK)){
                child.setTextColor(Color.RED);
                content.put(text, Color.RED);
            }else{
                child.setTextColor(Color.BLACK);
                content.put(text, Color.BLACK);
            }
            Log.d("found", "found");

        }else{
            Log.d("not found", "not found");
            child.setTextColor(Color.RED);
            content.put(text, Color.RED);
        }

    }

}
