package com.industries.stark.android1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class InputActivity extends Activity {

    public static final String EXTRA_MSG = "message";
    private Button clearButton;
    private EditText inputField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("SENDER","onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        clearButton = (Button) findViewById(R.id.clear);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputField = (EditText) findViewById(R.id.inputTextField);
                inputField.setText("");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_input, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void submitText(View view){
        Log.d("SENDER", "in submitText");
        Intent listIntent = new Intent(this, ListViewActivity.class);
        inputField = (EditText) findViewById(R.id.inputTextField);
        String msg = inputField.getText().toString();
        Log.d("SENDER","msg:"+msg);
        listIntent.putExtra(EXTRA_MSG, msg);
        startActivity(listIntent);

    }
}