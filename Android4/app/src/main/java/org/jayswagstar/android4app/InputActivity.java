package org.jayswagstar.android4app;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.Map;

public class InputActivity extends AppCompatActivity {

    public static String EXTRA_MSG= "com.example.mjay.android4app";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button _button = (Button) findViewById(R.id.button);
        _button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendText();
            }
        });

        Button _showbutton = (Button) findViewById(R.id.button2);
        _showbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("InputAvtivity", "SHOW");
                showText();
            }
        });

        ImageButton _calcbutton = (ImageButton) findViewById(R.id.button3);
        _showbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("InputAvtivity", "CALC");
                openCalculator();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_delete) {
            deleteSaves();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void sendText() {
        Intent myIntent = new Intent(this, ListViewActivity.class);
        EditText _editText = (EditText) findViewById(R.id.editText);
        String msg = _editText.getText().toString();
        myIntent.putExtra(EXTRA_MSG, msg);
        startActivity(myIntent);
    }

    private void showText() {
        Intent myIntent = new Intent(this, ListViewActivity.class);
        startActivity(myIntent);
    }

    private void openCalculator() {
        Intent myIntent = new Intent(this, CalculatorActivity.class);
        startActivity(myIntent);
    }

    /**
     *  Deletes the data on SharedPreferences
     */
    private void deleteSaves() {
        Log.d("Input", "deleteAll");
        SharedPreferences preferences =  getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
