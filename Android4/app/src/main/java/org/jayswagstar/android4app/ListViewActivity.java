package org.jayswagstar.android4app;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.R.layout.simple_list_item_1;

/**
 * Created by mjay on 22.01.16.
 */
public class ListViewActivity extends ListActivity {

    private final String KEY = "content";
    private final int MAXHISTORY = 5;

    //Hashmap (Text, Farbe)
    HashMap<String, Integer> content = new HashMap<String, Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        Log.d("ListView", "onCreate");
        super.onCreate(savedInstanceState);

        // Message über Intent bekommen und in Hashmap speichern
        Intent intent = getIntent();
        if(intent.getStringExtra(InputActivity.EXTRA_MSG)!=null){
            String msg = intent.getStringExtra(InputActivity.EXTRA_MSG);
            Log.d("ListView", "got msg "+ msg);
            content.put(msg, Color.BLACK);
        }

        // Load shared Preferences (zum Speichern ohne DB)
        SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        //Nur die ersten n Elemente der Liste hinzufügen
        for (int n = 1; n <=MAXHISTORY; n++) {
            String entry = pref.getString(KEY + n, null);
            if (entry != null) {
                //Wir wollen keine leeren Einträge in der Liste
                if(entry.equals("")){
                    Log.d("empty", "EMPTY");
                    entry = "LEER";
                }
                //Und auch keine doppelten
                if(content.containsKey(entry)){
                    Log.d("contains", entry);
                    entry = entry + System.currentTimeMillis();
                }
                content.put(entry, Color.BLACK);
            } else {
                break;
            }
        }
        //Add keySet to an ArrayList
        ArrayList keyList = new ArrayList(content.keySet());

        //Create ArrayAdapter and "setListAdapter"
        ArrayAdapter<String> adaptor = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, keyList);
        setListAdapter(adaptor);
    }

    //Wird beim schließen der Activity aufgerufen...
    @Override
    protected void onDestroy() {
        //Vorher noch die Liste in die SharedPreferences speichern
        int n = 1;
        SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        for (Map.Entry<String, Integer> entry : content.entrySet()) {
            editor.putString(KEY + n, entry.getKey());
            n++;
            if(entry.getKey()!=null) Log.d("RECEIVER",entry.getKey());
        }
        editor.commit();
        super.onDestroy();
    }

    //Wird beim Click auf ein Item der Liste aufgerufen
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        //Auswahl farbig markieren
        TextView child = (TextView) l.getChildAt(position);
        String text = child.getText().toString();
        Log.d("text", text);
            if(content.get(text).equals(Color.BLACK)){
                child.setTextColor(Color.GREEN);
                content.put(text, Color.GREEN);
            }else{
                child.setTextColor(Color.BLACK);
                content.put(text, Color.BLACK);
            }
    }
}
