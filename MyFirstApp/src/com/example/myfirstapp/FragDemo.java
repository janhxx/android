package com.example.myfirstapp;


import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.*;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FragDemo extends ListFragment {
	
	String[] entries = new String[]{"This", "That", "Everything else"};
	
	onListItemClickedListner mListener;
	
	public interface onListItemClickedListner{
		public void onListItemClicked(String text);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        /** Creating an array adapter to store the list of countries **/
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(inflater.getContext(), android.R.layout.simple_list_item_1,entries);
 
        /** Setting the list adapter for the ListFragment */
        setListAdapter(adapter);
 
        return super.onCreateView(inflater, container, savedInstanceState);
    }
	
	@Override
    public void onListItemClick(ListView l, View v, int position, long id) {
      mListener.onListItemClicked(entries[position]);
    }
	
	
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (onListItemClickedListner) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnArticleSelectedListener");
        }
    }




}
