package com.example.myfirstapp;



import com.example.myfirstapp.FragDemo.onListItemClickedListner;

import android.support.v4.app.FragmentActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class MyFirstActivity extends FragmentActivity implements onListItemClickedListner{

	public static final String EXTRA_MSG = "com.example.myfirstapp.MESSAGE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_first_layout);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_first, menu);
		
//		 FragmentManager fm = getFragmentManager();  
//		    
//		    if (fm.findFragmentById(android.R.id.content) == null) {  
//		     FragDemo list = new FragDemo();  
//		     fm.beginTransaction().add(android.R.id.content, list).commit();  
//		    }  
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void changeText(View view) {
		TextView textView = (TextView) findViewById(R.id.sendText);
		char[] text = textView.getText().toString().toCharArray();

		for (int i = 0; i < text.length; i++) {
			text[i] = (char) ((char) (text[i]+1)%127);			
		}
		StringBuilder builder = new StringBuilder();
		builder.append(text);
		textView.setText(builder.toString());
	}
	
	public void intendDemo (View view){
		
		Intent myIntent = new Intent(this, IntentDemo.class);
		TextView textView = (TextView) findViewById(R.id.sendText);
		String msg = textView.getText().toString();
		myIntent.putExtra(EXTRA_MSG,msg);
		startActivity(myIntent);
	}

	@Override
	public void onListItemClicked(String text) {
		TextView textView = (TextView) findViewById(R.id.sendText);
		textView.setText(text);
	}
	
}
